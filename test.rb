#!/usr/bin/env ruby
require 'socket'

sock = UDPSocket.new
sock.send "Foo/de]] xyz UTF-8 test arrow → (should be single-char ->)", 0, "127.0.0.1", 10010

# vim: ft=rb sts=2 ts=2:
