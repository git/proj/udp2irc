require 'socket'

class Listener
  def initialize(bot)
    @bot = bot
  end

  def start
    @server = UDPSocket.new
    @server.bind(INGEST_ADDR, INGEST_PORT)

    while true do
      msg, sender = @server.recvfrom(1024)
      @bot.handlers.dispatch(:udpmessage, nil, msg)
    end
  end
end

class MessagePlugin
  include Cinch::Plugin

  listen_to :udpmessage
  def listen(m, message)
    message.force_encoding('UTF-8') # All Gentoo is pure UTF-8
    FILTERS.each do |filter|
      return if message.match filter
    end

    Channel(IRC_CHANNEL).send message
  end
end

# vim: ft=rb sts=2 ts=2:
