#!/usr/bin/env ruby

require 'cinch'
require_relative 'plugins/message'
require_relative 'config'

bot = Cinch::Bot.new do
	loggers.level = :warn
	configure do |c|
		c.server = IRC_SERVER
		c.encoding = 'utf-8'
		c.channels = [IRC_CHANNEL]
		c.nick = IRC_NICK
		c.user = IRC_USER
		c.password = IRC_PASS
		c.realname = IRC_REALNAME
		c.plugins.plugins = [MessagePlugin]
	end
end

Thread.new { Listener.new(bot).start }
bot.start

# vim: ft=rb sts=2 ts=2:
